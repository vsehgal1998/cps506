import Data.Char
import Data.Array.IArray
import Data.List.Split

main = do
       input <- getLine
	
       putStrLn $ show $ readFrom input
	   boardVar = head (readFrom input)
	   rows = (splitOn " " boardVar) !! 1
	   columns = (splitOn " " boardVar) !! 2
	   board = boardCreate rows columns
	   
readFrom stdin = splitOn "\\n" stdin

board int1 int2 = do
	   let int3 = int2 
	   printBar int1 int2 int3
	   printBottom int1 int2

printBar n m x = do
	putStr "|"
	printSpaces n m x

printSpaces n 0 x = do
	putStrLn "|"
	printBar n x x

printSpaces n m x = do
	putStr " "
	printSpaces n (m-1) x
	
printBottom n m = do
	putStr "+" 
	printBottomDashes n m
	
printBottomDashes n m = do
	putStr "-"
	printBottomDashes n m-1
	
printBottomDashes n 0 = do
	putStrLn "+"